#!/bin/sh

if [ "$NODE_ENV" == "production" ] ; then
  npm run tsc
  npm run start
else
  npm run dev
fi
