import {MigrationInterface, QueryRunner} from "typeorm";

export class UserFullName1552252490978 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_c461d3719320bc0914e17f9d71e"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_edc91302c291cf01fd0e5e884bb"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP CONSTRAINT "user_contacts_user_userId_2_fkey"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP CONSTRAINT "user_contacts_user_userId_1_fkey"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "REL_edc91302c291cf01fd0e5e884b"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "to_balance_id"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "REL_c461d3719320bc0914e17f9d71"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "from_balance_id"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP CONSTRAINT "PK_a81491e712124db8d5423803ecb"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD CONSTRAINT "PK_70b58d86cb4e76666ddc307389f" PRIMARY KEY ("userId_1", "userId_2")`);
        await queryRunner.query(`ALTER TABLE "voucher" ALTER COLUMN "issued" SET DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE "voucher" ALTER COLUMN "created_at" SET DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE "voucher" ALTER COLUMN "updated_at" SET DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`CREATE INDEX "IDX_7476f44166ea089024ca3dd271" ON "user_contacts_user" ("userId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_90d50e9eaf2ba430c86acb7631" ON "user_contacts_user" ("userId_2") `);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD CONSTRAINT "FK_7476f44166ea089024ca3dd2719" FOREIGN KEY ("userId_1") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD CONSTRAINT "FK_90d50e9eaf2ba430c86acb76315" FOREIGN KEY ("userId_2") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP CONSTRAINT "FK_90d50e9eaf2ba430c86acb76315"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP CONSTRAINT "FK_7476f44166ea089024ca3dd2719"`);
        await queryRunner.query(`DROP INDEX "IDX_90d50e9eaf2ba430c86acb7631"`);
        await queryRunner.query(`DROP INDEX "IDX_7476f44166ea089024ca3dd271"`);
        await queryRunner.query(`ALTER TABLE "voucher" ALTER COLUMN "updated_at" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "voucher" ALTER COLUMN "created_at" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "voucher" ALTER COLUMN "issued" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" DROP CONSTRAINT "PK_70b58d86cb4e76666ddc307389f"`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD CONSTRAINT "PK_a81491e712124db8d5423803ecb" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "from_balance_id" integer`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "REL_c461d3719320bc0914e17f9d71" UNIQUE ("from_balance_id")`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "to_balance_id" integer`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "REL_edc91302c291cf01fd0e5e884b" UNIQUE ("to_balance_id")`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD CONSTRAINT "user_contacts_user_userId_1_fkey" FOREIGN KEY ("userId_1") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_contacts_user" ADD CONSTRAINT "user_contacts_user_userId_2_fkey" FOREIGN KEY ("userId_2") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_edc91302c291cf01fd0e5e884bb" FOREIGN KEY ("to_balance_id") REFERENCES "balance"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_c461d3719320bc0914e17f9d71e" FOREIGN KEY ("from_balance_id") REFERENCES "balance"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
