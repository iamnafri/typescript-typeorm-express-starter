import { default as AlphaTestReset } from './alpha-test-reset.cron';
import { default as PingTransactionWorkerCron } from './ping-transaction-worker.cron';
// import { default as ValidateTotalFundsCron } from './validate-total-funds.cron';
import logger from './../utils/logger';

export default class Cron {

  constructor() {
    logger.info({ message: 'cron.index constructor' });
  }

  init() {
    logger.info({ message: 'cron.index init' });
    new AlphaTestReset();
    new PingTransactionWorkerCron();
    // new ValidateTotalFundsCron();
  }

  scheduledJobs() {
    logger.info({ message: 'ScheduledJobs' });

  }
}
