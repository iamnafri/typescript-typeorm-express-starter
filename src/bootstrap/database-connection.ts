import { createConnection, ConnectionOptions, Connection } from 'typeorm';
import { config } from '../config';
import * as path from 'path';

export const databaseConnection = (): Promise<Connection> => {
  console.log('setting database options');
  const connectionOptions: ConnectionOptions = {
    name: 'default',
    type: 'postgres',
    host: config.db.host || 'localhost',
    port: Number(config.db.port) || 5432,
    username: config.db.username || 'postgres',
    password: config.db.password || 'password',
    database: config.db.database || 'dhm',
    logging: false,
    entities: [
      path.join(__dirname, '../') + '/**/*.entity{.ts,.js}'
    ],
    subscribers: [
      path.join(__dirname, '../') + '/**/*.subscriber{.ts,.js}'
    ],
    cli: {
      migrationsDir: path.join(__dirname, '../') + '/src/migrations',
    }
    // synchronize: true,
  };

  return createConnection(connectionOptions)
}
