import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from 'typeorm';
import User from './user.entity';
import Message from './../message/message.entity';
import CreateMessageDto from './../message/message.dto';
import Balance from './../balance/balance.entity';
import CreateBalanceDto from './../balance/balance.dto';
import logger from './../../utils/logger';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {

  listenTo() {
    return User;
  }

  async afterInsert(event: InsertEvent<User>) {

    // Initiate Balance
    try {
      const balanceData: CreateBalanceDto = {
        amount_meta: [0,0,0,0,0,0],
        user: event.entity,
        transaction: null
      }
      const balance: Balance = await event.manager.getRepository(Balance).create({ ...balanceData });
      await event.manager.getRepository(Balance).save(balance);
    } catch (err) {
      logger.warn(err);
    }

    // Create a new message when an existing user is provided
    try {
      const user: User = event.entity;
      const messageData: CreateMessageDto = {
        type: 'system',
        user: user,
        subject: 'Welkom!',
        body: `
              <p>Hoi ${user.username}!</p>
              <br>
              <p>Je account op De Haagse Munt is aangemaakt! Je kunt nu inloggen en tegoed ontvangen via je gebruikersnaam: ${user.username}. </p>
              <br>
              <p>Je ontvangt snel een user met startkrediet. </p>
              <br>
              <br>
              <p>PS, je ontvangt dit bericht omdat je alpha tester bent van De Haagse Munt. De transacties die je uitvoert zijn nog niet definitief. Iedere nacht wordt het systeem gereset en ontvang je opnieuw startkrediet.`
      }
      const msg: Message = await event.manager.getRepository(Message).create({...messageData});
      await event.manager.getRepository(Message).save(msg);

    } catch(err) {
      logger.warn(err);
    }
  }
}
