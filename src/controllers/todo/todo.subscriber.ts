import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from 'typeorm';
import Todo from './../todo/todo.entity';
import logger from './../../utils/logger';

@EventSubscriber()
export class TodoSubscriber implements EntitySubscriberInterface<Todo> {

  listenTo() {
    return Todo;
  }

  // Create a new balance after inserting a new user.
  async afterInsert(_event: InsertEvent<Todo>) {

    logger.debug('todo.subscriber] - AfterInsert hook');
    
  }
}
